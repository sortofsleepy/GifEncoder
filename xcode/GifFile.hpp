//
//  GifFile.hpp
//  GifEncoder
//  Ported from work done by Jesus Gollonet as part of ofxGifDecoder
//  https://github.com/jesusgollonet/ofxGifDecoder
//  Created by Joseph Chow on 3/25/16.
//
//

#ifndef GifFile_hpp
#define GifFile_hpp

#include "GifFrame.hpp"

enum GifFrameDisposal {
    GIF_DISPOSAL_UNSPECIFIED,
    GIF_DISPOSAL_LEAVE,
    GIF_DISPOSAL_BACKGROUND,
    GIF_DISPOSAL_PREVIOUS
};


class GifFile {
    
    ci::ColorA bgColor;
    std::vector <GifFrame > gifFrames;
    std::vector <ci::gl::TextureRef> rawFrames;
    std::vector <ci::Color> globalPalette;
    //vector <ofPixels *> rawPixels;
    int w, h, nPages;
    bool bAnimated;
    bool bLoop;
    float duration;
    ci::Surface8uRef accumPx;
    float gifDuration;
public:
    GifFile();
    ~GifFile();
    void setup(int _w, int _h, std::vector<ci::Color> _globalPalette, int _nPages);
    void setBackgroundColor(ci::Color _c);
    ci::Color getBackgroundColor();
    void addFrame(ci::Surface8uRef _px, int _left , int _top, GifFrameDisposal disposal = GIF_DISPOSAL_PREVIOUS, float _duration = 0);
    std::vector <ci::Color> getPalette();
    // void numFrames, void isAnimated, void duration
    int getNumFrames();
    int getWidth();
    int getHeight();
    float getDuration();
    
    
    GifFrame * getFrameAt(int _index);
    // array operator overload?
    // gif[1] is frame 1, and we can treat is as such
    // gif[1].getTop(); gif[1].draw() ...
    
    // void update
    void draw(float _x, float _y);
    // this should draw with the offsets correctly applied.
    void drawFrame(int _frameNum, float _x, float _y);
    void drawFrame(int _frameNum, float _x, float _y, int _w, int _h);
    void clear();

};

#endif /* GifFile_hpp */
