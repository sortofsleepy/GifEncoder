#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "ciGiphy.h"
using namespace ci;
using namespace ci::app;
using namespace std;

class GifEncoderApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
    	void keyDown( KeyEvent event ) override;
	void update() override;
	void draw() override;
    void encodeDone();
    GifEncoder encoder;
   
    std::vector<ci::SurfaceRef> images;
};

void GifEncoderApp::encodeDone()
{
    app::console()<<"Encoding done \n";
}


void GifEncoderApp::setup()
{
    
    for(int i = 0; i < 18; ++i){
        Surface8uRef surf = Surface::create(loadImage(loadAsset("sequence/test" + to_string(i) + ".jpg")));
        images.push_back(surf);
    }
    
    auto img = images.at(0);
  
    encoder.setup(640, 480);
    encoder.setFilename("/Users/sortofsleepy/documents/test/poop_test.gif");
    encoder.addFrames(images);
    //encoder.addFrame(img->getData(), 640, 480);
    
    encoder.onEncodeComplete(&GifEncoderApp::encodeDone, this);
    
    encoder.doSave();
    
    
    
    
}

void GifEncoderApp::mouseDown( MouseEvent event )
{
}


void GifEncoderApp::keyDown( KeyEvent event )
{
 
    //writeImage("test" + std::to_string(frameCount) + ".jpg", copyWindowSurface());
    //frameCount += 1;
}

void GifEncoderApp::update()
{
}

void GifEncoderApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
    gl::setMatricesWindow(getWindowSize());
   
    
   
}

CINDER_APP( GifEncoderApp, RendererGl )
