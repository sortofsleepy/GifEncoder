//
//  GFrame.cpp
//  GifEncoder
//
//  Created by Joseph Chow on 3/16/16.
//
//

#include "GifFrame.hpp"
using namespace ci;
using namespace std;

GifFrame::GifFrame(){}

GifFrame::GifFrame(unsigned char * px, int _w, int _h, int _bitsPerPixel, float _duration){
    left = top = 0;
    duration = _duration;
    pixels = px;
    width = _w;
    height = _h;
    bitsPerPixel = _bitsPerPixel;
}

void GifFrame::setFromPixels(ci::Surface8uRef img, int _left, int _top,float _duration){
    frameImage = img;
    left = _left;
    top = _top;
    duration = _duration;
    pixels = img->getData();
    tx = gl::Texture::create(img->getData(), GL_RGB, img->getWidth(), img->getHeight());
}

unsigned char * GifFrame::getRawPixels(){
    return frameImage->getData();
}